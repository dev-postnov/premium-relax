//https://github.com/desandro/masonry

//START REVIEWS LIST

window.addEventListener('load', function() {

    if (document.querySelector('.reviews__list')) {
        // vanilla JS
        // init with element
        var reviews = document.querySelector('.reviews__list');


        var msnryReviews = new Masonry( reviews, {
            // options...
            itemSelector: '.reviews__item',
        });

    }

});



//END REVIEWS LIST
