//START NAVIGATION


    var nav      = document.querySelector('.nav'),
        navBtn   = document.querySelector('.nav__toggle'),
        navItems = document.querySelectorAll('.nav__item');


    navBtn.addEventListener('click', function(e) {
        e.stopPropagation();

        var navItemsLength = navItems.length;


        if (nav.classList.contains('is-active')) {

            nav.classList.remove('is-active');

            for (var i = 0; i < navItemsLength; i++) {
                navItems[i].classList.remove('is-active');
            }

        }else{
            nav.classList.add('is-active');

            setTimeout(function() {

                for (var i = 0; i < navItemsLength; i++) {
                    (function(i) {
                        setTimeout(function() {
                            navItems[i].classList.add('is-active')
                        },i * 200)
                    })(i)
                }

            }, 300);
        }

    }, false);


//END NAVIGATION