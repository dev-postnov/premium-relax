// START CONTACT BLOCK


    function initMap() {
        var map = new google.maps.Map(document.getElementById('contact-map-1'), {
            zoom: 17,
            center: {
                lat: 45.04523743,
                lng: 38.97968983
            }
        });

        var map2 = new google.maps.Map(document.getElementById('contact-map-2'), {
            zoom: 17,
            center: {
                lat: 45.04523743,
                lng: 38.97968983
            }
        });

        var marker = new google.maps.Marker({
            position: {
                lat: 45.04523743,
                lng: 38.97968983
            },
            map: map
        });

        var marker2 = new google.maps.Marker({
            position: {
                lat: 45.04523743,
                lng: 38.97968983
            },
            map: map2
        });

    }


// END CONTACT BLOCK