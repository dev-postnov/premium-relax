//https://github.com/desandro/masonry

//START GIRL LIST


window.addEventListener('load', function() {

    if (document.querySelector('.girls__list')) {

        // vanilla JS
        // init with element
        var girls = document.querySelector('.girls__list');


        var msnryGirls = new Masonry( girls, {
            // options...
            itemSelector: '.girls__item',
        });
    }

});


//END GIRL LIST
