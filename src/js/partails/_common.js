// START COMMON


    // lazy load image
    var bLazy = new Blazy({
        offset: 50
    });



    // delete after text for input type 'date', because some browser support type 'date'

    if (document.querySelector('.placeholder')) {

        document.querySelector('.placeholder').addEventListener('tap', function(e) {
            e.target.classList.add('non-empty');
        });

    }




    if (document.querySelector("#phone")) {

        document.querySelector("#phone").addEventListener('focus', function(e) {
            e.target.value = '+7';
        }, false);

        VMasker(document.querySelector("#phone")).maskPattern("+9(999) 999-99-99");
    }


// END COMMON