'use strict';

var gulp        = require('gulp'),
    watch       = require('gulp-watch'),
    prefixer    = require('gulp-autoprefixer'),
    uglify      = require('gulp-uglify'),
    sass        = require('gulp-sass'),
    rigger      = require('gulp-rigger'),
    gutil       = require('gulp-util'),
    ftp         = require('gulp-ftp'),
    pug         = require('gulp-pug'),
    cssmin      = require('gulp-minify-css'),
    rimraf      = require('rimraf'),
    csscomb     = require('gulp-csscomb'),
    browserSync = require('browser-sync'),
    media_group = require('gulp-group-css-media-queries'),
    rename      = require('gulp-rename'),
    criticalCss = require('gulp-critical-css'),
    replace     = require('gulp-replace'),
    flatten     = require('gulp-flatten'),
    plumber     = require('gulp-plumber'),
    imports     = require('gulp-imports'),
    prettyHtml  = require('gulp-pretty-html'),
    reload      = browserSync.reload;




var path = {
    dist: {
        html: 'dist/',
        js: 'dist/js/',
        css: 'dist/',
        img: 'dist/images/',
        fonts: 'dist/fonts/',
        blocks: 'dist/'
    },
    src: {
        pug: ['!src/**/_*', 'src/*.pug'],
        pugBlocks: ['!src/**/_*','!src/*.pug', 'src/**/*.pug'],
        css: 'src/template_styles.scss',
        cssBlocks: ['!src/**/_*','!src/template_styles.scss', 'src/**/*.scss'],
        js: 'src/js/main.js',
        jsBlocks: ['!src/**/_*','!src/js/main.js', 'src/**/*.js'],
        img: 'src/**/images/**/*.*',
        imagesAll: 'src/images/*.*',
        fonts: 'src/fonts/**/*.*'
    },
    watch: {
        js: 'src/**/*.js',
        pug: 'src/**/*.pug',
        css: 'src/**/*.scss',
        img: 'src/**/images/**.*',
        fonts: 'src/fonts/**/*.*'
    },
    clean: './dist'
};



var config = {
    server: {
        baseDir: './dist'
    },
    tunnel: true,
    host: 'localhost',
    port: 3000,
    logPrefix: 'premium-relax'
};


gulp.task('deploy', function () {
    return gulp.src('dist/**/*')
        .pipe(ftp({
            host: '*hostname*',
            user: '*username*',
            pass: '*password*',
            remotePath: '*/path/path*'
        }))
        .pipe(gutil.noop());
});




gulp.task('pug:build', function() {
    return gulp.src(path.src.pug)
        .pipe(pug())
        .on('error', function (err) {
            console.log(err.toString());
            this.emit('end');
        })
        .pipe(prettyHtml({
            indent_size: 4,
            indent_char: ' ',
            unformatted: ['code', 'pre', 'em', 'strong', 'i', 'b', 'br', 'style']
        }))
        .pipe(plumber())
        .pipe(gulp.dest(path.dist.html))
        .pipe(reload({ stream: true }));
})



gulp.task('pug:blocks', function() {
    return gulp.src(path.src.pugBlocks)
        .pipe(pug({
            pretty: true
        }))
        .on('error', function (err) {
            console.log(err.toString());
            this.emit('end');
        })
        .pipe(plumber())
        .pipe(gulp.dest(path.dist.blocks))
        .pipe(reload({ stream: true }));
})

gulp.task('js:build', function() {
    return gulp.src(path.src.js)
        .pipe(imports())
        .on('error', function (err) {
            console.log(err.toString());
            this.emit('end');
        })
        .pipe(gulp.dest(path.dist.js))
        .pipe(uglify())
        .pipe(rename({suffix:'.min'}))
        .pipe(gulp.dest(path.dist.js))
        .pipe(reload({ stream: true }));
});


gulp.task('js:blocks', function() {
    return gulp.src(path.src.jsBlocks)
        .pipe(imports())
        .on('error', function (err) {
            console.log(err.toString());
            this.emit('end');
        })
        .pipe(gulp.dest(path.dist.blocks))
        .pipe(uglify())
        .pipe(rename({suffix:'.min'}))
        .pipe(plumber())
        .pipe(gulp.dest(path.dist.blocks))
});




gulp.task('css:build', function() {
    return gulp.src(path.src.css)
        .pipe(sass())
        .on('error', function (err) {
            console.log(err.toString());
            this.emit('end');
        })
        .pipe(plumber())
        .pipe(prefixer({
            browsers: ['last 15 versions'],
            cascade: false,
            grid: true
        }))
        .pipe(csscomb())
        .pipe(media_group())
        // .pipe(replace('../images/','images/'))
        .pipe(plumber())
        .pipe(gulp.dest(path.dist.css))
        .pipe(rename({suffix: '.min'}))
        .pipe(cssmin())
        .pipe(gulp.dest(path.dist.css))
        .pipe(reload({ stream: true }));
});


gulp.task('css:critical', function() {
    return gulp.src('dist/template_styles.css')
        .pipe(criticalCss())
        .on('error', function (err) {
            console.log(err.toString());
            this.emit('end');
        })
        .pipe(rename({suffix: '.min'}))
        .pipe(replace('../images/','images/'))
        .pipe(plumber())
        .pipe(cssmin())
        .pipe(gulp.dest(path.dist.css))
})

gulp.task('css:blocks', function() {
    return gulp.src(path.src.cssBlocks)
        .pipe(sass())
        .on('error', function (err) {
            console.log(err.toString());
            this.emit('end');
        })
        .pipe(prefixer({
            browsers: ['last 15 versions'],
            cascade: false,
            grid: true
        }))
        .pipe(csscomb())
        .pipe(media_group())
        .pipe(plumber())
        .pipe(gulp.dest(path.dist.blocks))
})


gulp.task('images:build', function() {
    gulp.src([path.src.img, path.src.imagesAll])
        .pipe(flatten({ includeParents: 0}))
        .pipe(plumber())
        .pipe(gulp.dest(path.dist.img))
});

gulp.task('images:blocks', function() {
    gulp.src(path.src.img)
        .pipe(plumber())
        .pipe(gulp.dest(path.dist.blocks))
});





gulp.task('fonts:build', function() {
    gulp.src(path.src.fonts)
        .pipe(gulp.dest(path.dist.fonts))
});


gulp.task('build', [
    'pug:build',
    'js:build',
    'css:build',
    'images:build',
    'fonts:build',
    'css:critical',
    'images:blocks',
])


gulp.task('blocks', [
    'pug:blocks',
    'css:blocks',
    'js:blocks',
    'images:blocks'
])


gulp.task('watch', ['build'], function () {
    gulp.watch(path.watch.css, ['css:build', 'css:critical',])
    gulp.watch(path.watch.js, ['js:build'])
    gulp.watch(path.watch.pug, ['pug:build'] )
    gulp.watch(path.watch.img, ['images:build', 'images:blocks'])
})

gulp.task('webserver', function() {
    browserSync(config);
});

gulp.task('clean', function(cb) {
    rimraf(path.clean, cb);
});

gulp.task('default', ['build', 'webserver', 'watch']);
